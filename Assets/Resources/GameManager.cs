﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Sound Volumes")]
    [Range(0f, 1f)]
    public float soundEffectVolume;
    [Range(0f, 1f)]
    public float musicVolume;

    [Header("Game Speed")]
    [Range(0f, 2f)]
    public float gameSpeed;
    public bool lockActions;

    public static bool firstTimeLaunch = true; // TESTGJP
    public static int money = 0;
    public static int drawNumber = 2;
    public static int armourAmount = 0;
    public static int damageIncrease = 0;
    public static int numSlots = 1;
    public static int numReFlips = 0;
    public static int allyHealth = 6;

    // Hub Unlockeds
    public static int numFights = 1;
    public static int blackSmithVisit = 0;
    public static int witchVisit = 0;

    [SerializeField]
    public static List<CoinBase> startingCoins = new List<CoinBase>();

    public static string enemyRun = "1,2,3,4,5";

    public List<string> allEnemyRuns = new List<string>();

    public Fade sceneFader;

    #region Constructor
    private static GameManager _i;
    public static GameManager Instance
    {
        get
        {
            if (_i == null)
            {
                _i = Instantiate(Resources.Load<GameManager>("GameManager"));
            }

            return _i;
        }
    }
    #endregion Constructor

    #region Time Methosd

    // TODO smooth transition
    public void SetGameSpeed(float speed)
    {
        gameSpeed = speed;
    }

    #endregion Time Methosd


    public void FadeIn()
    {
        if (sceneFader)
            sceneFader.FadeIn();
    }
}
