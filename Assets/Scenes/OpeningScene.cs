 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Yarn.Unity;

public class OpeningScene : MonoBehaviour
{
    public GameObject Coin;
    public void FinishDialogue()
    {
        StartCoroutine(FadeToBlack());
    }

    [YarnCommand("fadeInCoin")]
    public void FadeInCoin()
    {
        Coin.SetActive(true);
    }


    [YarnCommand("end")]
    public void End()
    {
        Application.Quit();
    }

    IEnumerator FadeToBlack()
    {
        GameManager.Instance.FadeIn();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("HubArea");
        yield return null;
    }


}
