using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnMarker : MonoBehaviour
{
    public GameObject flipPosition;
    public Vector3 targetPosition;
    public Vector3 startPos;
    public float speed;
    public float distanceMultiplyer;

    public bool isAllySprite;
    public Sprite allyTurnSprite;
    public Sprite enemyTurnSprite;

    public bool predetermineAlly;
    public bool predetermineEnemy;

    private void Awake()
    {
        targetPosition = this.transform.position;
        startPos = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!((Vector2)this.transform.position).Equals(targetPosition))
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, targetPosition, (speed +
                (Vector2.Distance(this.transform.position, targetPosition) * distanceMultiplyer)));
        }
    }

    public void SetSprite(bool isAlly)
    {
        isAllySprite = isAlly;
        if (isAlly)
        {
            this.GetComponent<Image>().sprite = allyTurnSprite;
        }
        else
        {
            this.GetComponent<Image>().sprite = enemyTurnSprite;
        }
    }

    private void OnEnable()
    {
        this.transform.position = startPos;
        this.targetPosition = flipPosition.transform.position;
    }

    public IEnumerator Flip()
    {
        ActionOrganiser org = GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>();
        EnemyEntity enemy = org.spawner.SpawnEntityByID(org.enemyRunIDs[0]);
        org.turnMarker.targetPosition = startPos;
        org.enemyRunIDs.RemoveAt(0);
        this.gameObject.SetActive(false);
        yield return enemy.StartDialogue();
        this.gameObject.SetActive(true);
        org.turnMarker.targetPosition = flipPosition.transform.position;
        yield return new WaitForSeconds(2f);
        if (predetermineAlly)
        {
            SetSprite(true);
            org.StartTurn(org.allyEntity);
        }
        else if (predetermineEnemy)
        {
            SetSprite(false);
            org.StartTurn(org.enemyEntity);
        }
        else
        {
            org.lockTurnStarting = false;
            if (Random.value > 0.5f)
            {

                SetSprite(true);
                org.StartTurn(org.allyEntity);
            }
            else
            {
                SetSprite(false);
                org.StartTurn(org.enemyEntity);
            }
        }
        org.flippingTurnRoutine = false;
    }
}
