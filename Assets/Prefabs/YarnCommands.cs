using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class YarnCommands : MonoBehaviour
{
    public GameObject allyPos;
    public GameObject enemyPos;
    public GameObject box;

    [YarnCommand("setally")]
    public void SetAlly()
    {
        box.transform.position = allyPos.transform.position;
    }

    [YarnCommand("setenem")]
    public void SetEnem()
    {
        box.transform.position = enemyPos.transform.position;
    }
}
