using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEntitySpawner : MonoBehaviour
{
    public GameObject spawnLocation;
    public GameObject enemyPoint;
    [SerializeField]
    List<EntityPair> entitiesList = new List<EntityPair>();

    public EnemyEntity SpawnEntityByID(int id)
    {
        foreach(EntityPair p in entitiesList)
        {
            if (p.id == id)
            {
                GameObject enemy = Instantiate(p.spawnedEntity.gameObject);
                enemy.transform.position = spawnLocation.transform.position;
                enemy.GetComponent<EnemyEntity>().targetPosition = enemyPoint.transform.position;
                return enemy.GetComponent<EnemyEntity>();
            }
        }
        return null;
    }
}

[Serializable]
public class EntityPair
{
    public int id = 0;
    public Entity spawnedEntity;
}