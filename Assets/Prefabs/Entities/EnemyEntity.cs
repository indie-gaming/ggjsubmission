using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class EnemyEntity : Entity
{
    private DialogueRunner dr;
    public string dialogueStartNode;
    public string dialogueEndNode;
    public int moneyOnDeath;
    public Vector3 targetPosition;
    [Header("Spawn Move In")]
    public float speed = 1;
    public float distanceMultiplyer = 0.1f;
    [SerializeField]
    List<AttackActionPair> actions = new List<AttackActionPair>();

    public override void Start()
    {
        base.Start();
        if (orginiser.enemyEntity != this)
        {
            orginiser.enemyEntity = this;
        }
        dr = GameObject.FindGameObjectWithTag("DialogueRunner").GetComponent<DialogueRunner>();
        StartCoroutine(SpawnMoveIn());
    }


    public virtual IEnumerator SpawnMoveIn()
    {
        while (this.transform.position != targetPosition)
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, targetPosition, (speed +
                (Vector2.Distance(this.transform.position, targetPosition) * distanceMultiplyer)));
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    public IEnumerator MoveToTarget()
    {
        while (this.transform.position != targetPosition)
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, targetPosition, (speed +
                (Vector2.Distance(this.transform.position, targetPosition) * distanceMultiplyer)));
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    public IEnumerator EnemyAttack()
    {

        yield return DoAttack();

        orginiser.StartTurn(orginiser.allyEntity);
    }

    public bool dialogueRunning = false;

    public IEnumerator StartDialogue()
    {
        yield return new WaitForSeconds(0.5f); // wait to enter battlefield

        if (!string.IsNullOrEmpty(dialogueStartNode))
        {
            if (dr == null)
                dr = GameObject.FindGameObjectWithTag("DialogueRunner").GetComponent<DialogueRunner>();

            dr.StartDialogue(this.dialogueStartNode);
            while (dr.IsDialogueRunning)
            {
                yield return new WaitForEndOfFrame();
            }
        }
    }


    public IEnumerator EndDialogue()
    {
        yield return new WaitForSeconds(0.5f); // wait to enter battlefield

        if (!string.IsNullOrEmpty(dialogueStartNode))
        {
            if (dr == null)
                dr = GameObject.FindGameObjectWithTag("DialogueRunner").GetComponent<DialogueRunner>();

            dr.StartDialogue(this.dialogueStartNode);
            while (dr.IsDialogueRunning)
            {
                yield return new WaitForEndOfFrame();
            }
        }
    }

    public IEnumerator AttackDialogue(string node)
    {
        yield return new WaitForSeconds(0.5f); // wait to enter battlefield

        if (!string.IsNullOrEmpty(dialogueStartNode))
        {
            if (dr == null)
                dr = GameObject.FindGameObjectWithTag("DialogueRunner").GetComponent<DialogueRunner>();

            dr.StartDialogue(node);
            while (dr.IsDialogueRunning)
            {
                yield return new WaitForEndOfFrame();
            }
        }
    }

    public virtual IEnumerator DoAttack()
    {
        if (actions.Count > 0)
        {
            AttackActionPair attack = actions[UnityEngine.Random.Range(0, actions.Count - 1)];
            if (!string.IsNullOrEmpty(attack.node))
                yield return AttackDialogue(attack.node);

            if (this.GetComponent<Animator>() != null)
            {
                this.GetComponent<Animator>().Play("EnemyAttack");
                attack.act.DoEffect();
            }
            Camera.main.GetComponent<AudioSource>().PlayOneShot(GameAssets.Instance.soundAudioClips[0]);
        }

        yield return new WaitForSeconds(2f);
    }
}

[Serializable]
public class AttackActionPair
{
    public Action act;
    public string node = null;
}
