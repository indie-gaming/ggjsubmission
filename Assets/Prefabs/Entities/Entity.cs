using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public float maxHealth;
    public float currentHealth;
    protected ActionOrganiser orginiser;

    [SerializeField]
    public List<Action> startTurnActions = new List<Action>();

    public int poisonDamageAmount = 0;

    public  virtual void Start()
    {
        orginiser = GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>();
    }

    public virtual void TurnStart()
    {
        this.currentHealth -= poisonDamageAmount;
        foreach (Action action in startTurnActions)
        {
            action.DoEffect();
        }
    }

    public virtual void TakeHit()
    {
    }
}
