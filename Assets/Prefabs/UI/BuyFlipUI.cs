using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuyFlipUI : MonoBehaviour
{
    public int costOfFlip = 2;
    public TMP_Text costText;
    public Button buyButton;

    private void Start()
    {
        costText.text = Mathf.Pow(costOfFlip, GameManager.numReFlips + 1).ToString();
        buyButton.interactable = GameManager.money >= Mathf.Pow(costOfFlip, GameManager.numReFlips + 1) / 2;
    }
    private void OnEnable()
    {
        costText.text = Mathf.Pow(costOfFlip, GameManager.numReFlips + 1).ToString();
        buyButton.interactable = GameManager.money >= Mathf.Pow(costOfFlip, GameManager.numReFlips + 1) / 2;
    }

    public void TryBuyFlip()
    {
        if (GameManager.money >= Mathf.Pow(costOfFlip, GameManager.numReFlips + 1) / 2)
        {
            GameManager.numReFlips++;
            GameManager.money -= (int)Mathf.Pow(costOfFlip, GameManager.numReFlips + 1) / 2;
            costText.text = (Mathf.Pow(costOfFlip, GameManager.numReFlips + 1) / 2).ToString();
            buyButton.interactable = GameManager.money >= Mathf.Pow(costOfFlip, GameManager.numReFlips + 1) / 2;
        }
    }
}
