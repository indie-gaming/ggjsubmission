using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBagShake : MonoBehaviour
{
    public void ShakeCoinBag()
    {
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().SpawnCoins();
    }
}
