using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialNext : MonoBehaviour
{
    public TMP_Text textMP;
    public GameObject nextButton;
    public GameObject nextTutorial;
    private bool canNext = false;

    private string tutText;

    private void Awake()
    {
        nextButton.SetActive(false);
        tutText = textMP.text;
        textMP.text = "";
        StartCoroutine(ScrollText());
    }

    private void Update()
    {
        if (canNext && Input.anyKeyDown)
        {
            if (nextTutorial != null)
                nextTutorial.SetActive(true);
            else
            {
                GameManager.Instance.lockActions = false;
            }
            this.gameObject.SetActive(false);
        }
    }

    IEnumerator ScrollText()
    {
        int i = 0;
        while (i < tutText.Length)
        {
            if (tutText[i] == ' ')
            {
                textMP.text = textMP.text + tutText[i];
                i++;
            }

            textMP.text = textMP.text + tutText[i];
            i++;
            yield return new WaitForSeconds(0.03f);
        }
        nextButton.SetActive(true);
        canNext = true;
        yield return null;
    }
}
