using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CoinSlot : MonoBehaviour, IDropHandler
{
    public CoinBase currentCoin = null;
    public UIParticleSystem uiParticles;

    public void OnDrop(PointerEventData eventData)
    {
        if (currentCoin == null)
        {
            CoinBase draggedCoin = eventData.pointerDrag.GetComponent<CoinBase>();
            draggedCoin.returnPosition =  new Vector2(0,0);
            draggedCoin.transform.SetParent(this.transform);
            currentCoin = draggedCoin;
            draggedCoin.slot = this;
            GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().ValidateCoinRun();
        }
    }

    public void PlayParticles()
    {
        uiParticles.Play();
    }
}
