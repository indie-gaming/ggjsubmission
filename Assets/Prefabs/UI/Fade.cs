using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{

    private void Start()
    {
        GameManager.Instance.sceneFader = this;
    }
    public void FadeFinished()
    {
        this.gameObject.SetActive(false);
    }

    public void FadeIn()
    {
        this.gameObject.SetActive(true);
        this.GetComponent<Animator>().Play("FadeIn");
    }
}
