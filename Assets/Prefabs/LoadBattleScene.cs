using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadBattleScene : MonoBehaviour
{

    public void LoadBattleZone()
    {
        //Fix fade in
        StartCoroutine(FadeToBlack());
    }


    IEnumerator FadeToBlack()
    {
        GameManager.Instance.FadeIn();
        yield return new WaitForSeconds(1f);
        GameManager.numFights++;
        SceneManager.LoadScene("FightScene");
        yield return null;
    }
}
