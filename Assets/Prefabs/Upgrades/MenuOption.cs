using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuOption : MonoBehaviour
{
    public Button button;
    public int cost;
    public TMP_Text money;


    public void Awake()
    {
        money.text = cost.ToString();
    }

    public void Update()
    {
        button.interactable = GameManager.money >= cost;
    }

    public void ClickSuccess()
    {
        GameManager.money -= cost;
    }

}
