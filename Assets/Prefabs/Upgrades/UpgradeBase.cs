using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeBase : MonoBehaviour
{
    public MenuOption damageUpgrade1;
    public MenuOption damageUpgrade2;
    public MenuOption damageUpgrade3;

    public MenuOption AddCoinDamage1;
    public MenuOption AddCoinDamage2;

    public MenuOption AddSlot1;
    public MenuOption AddSlot2;
    public MenuOption AddSlot3;

    public void Update()
    {
        if (GameManager.damageIncrease == 0 && GameManager.numFights > 2)
        {
            damageUpgrade1.gameObject.SetActive(true);
        }
        if (GameManager.damageIncrease == 1 )
        {
            damageUpgrade2.gameObject.SetActive(true);
        }
        if (GameManager.damageIncrease == 2)
        {
            damageUpgrade3.gameObject.SetActive(true);
        }
        if (GameManager.numFights >= 1 && GameManager.startingCoins.Count == 2)
        {
            AddCoinDamage1.gameObject.SetActive(true);
        }
        if (GameManager.startingCoins.Count > 2)
        {
            AddCoinDamage2.gameObject.SetActive(true);
        }
        // Add Slot
        if (GameManager.drawNumber > 2 && GameManager.numSlots == 1)
        {
            AddSlot1.gameObject.SetActive(true);
        }
        if (GameManager.drawNumber > 4 && GameManager.numSlots == 2)
        {
            AddSlot2.gameObject.SetActive(true);
        }
        if (GameManager.drawNumber > 5 && GameManager.numSlots == 3)
        {
            AddSlot3.gameObject.SetActive(true);
        }
    }

    public virtual void UnlockUpgrade(int unlockType)
    {
        Debug.Log("unlocked " + unlockType);
        if (unlockType == 0)
        {
            GameManager.damageIncrease++;
        }
        if (unlockType == 1)
        {
            // add new coin to game manager
            GameManager.startingCoins.Add(GameManager.startingCoins[0]);
            GameManager.drawNumber++;
        }
        if (unlockType == 2)
        {
            GameManager.numSlots++;
        }
        if (unlockType == 3)
        {
            GameManager.allyHealth++;
        }
    }


}
