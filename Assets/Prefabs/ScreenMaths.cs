using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScreenMaths
{
    public static Vector2 ConvertScreenToCanvasPosition(Canvas canvas, Vector2 pos)
    {
        Vector2 convertedPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, pos, canvas.worldCamera, out convertedPos);
        return convertedPos;
    }

    public static Vector2 ConvertWorldPositionToScreenCanvasPosition(Camera cam, Canvas canvas, Vector2 pos)
    {
        return ConvertScreenToCanvasPosition(canvas, cam.WorldToScreenPoint(pos));
    }


}
