using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ActionOrganiser : MonoBehaviour
{
    public RectTransform coinArea = null;
    public RectTransform discardArea = null;
    [Header("Coin Bag")]
    public CoinBase coinBase;
    public List<CoinBase> coinBagCoins = new List<CoinBase>();
    public CoinBagShake coinBagObject;
    public int drawNum = 3;
    [Space(10)]

    public Button attackButton;

    public int numberOfSlots = 3;
    public GameObject coinSlotBase;
    public Vector2 coinSlotMargin;
    private List<CoinSlot> coinRun = new List<CoinSlot>();

    [Space(10)]
    [Header("Health Bar UI")]
    public Slider allyHealth;
    public Slider enemyHealth;
    public GameObject allyTurnMarkerPos;
    public GameObject enemyTurnMarkerPos;
    public TurnMarker turnMarker;
    [Header("Battling Entities")]
    public PlayerEntity allyEntity;
    public EnemyEntity enemyEntity;

    [Header("Flip Variables")]
    public int numRemainingFlips;
    public List<GameObject> flipIcons = new List<GameObject>();
    public GameObject elipseFlipIcon;
    public GameObject tutorial;

    public bool flippingTurnRoutine;
    public bool lockTurnStarting;

    private Entity currentEntityTurn;
    public List<int> enemyRunIDs = new List<int>();
    public EnemyEntitySpawner spawner;

    [Header("Poison Icons")]
    public GameObject allyPoisonIcon;
    public TMP_Text allyPoisonText;
    public GameObject enemyPoisonIcon;
    public TMP_Text enemyPoisonText;
    [Header("Coin Animator")]
    public RuntimeAnimatorController coinAnimator;
    public GameObject coinText;
    private bool tutorialLock = false;

    private void Awake()
    {
        LoadStartingCoins();
        coinArea = GameObject.FindGameObjectWithTag("CoinArea").GetComponent<RectTransform>();
        currentEntityTurn = allyEntity;
        numberOfSlots = GameManager.numSlots;

        if (GameManager.firstTimeLaunch)
            enemyRunIDs.Add(0);
        foreach (string numStr in GameManager.enemyRun.Split(','))
        {
            enemyRunIDs.Add(int.Parse(numStr));
        }


        ResizeSlotArea();
    }


    public void Update()
    {
        if (GameManager.firstTimeLaunch)
        {
            GameManager.firstTimeLaunch = false;
            GameManager.Instance.lockActions = true;
            tutorial.SetActive(true);
            turnMarker.predetermineAlly = true;
        }

        if (numberOfSlots != coinRun.Count)
            ResizeSlotArea();

        if (currentEntityTurn != null)
            turnMarker.targetPosition = allyEntity == currentEntityTurn ? allyTurnMarkerPos.transform.position : enemyTurnMarkerPos.transform.position;
        else if (!GameManager.Instance.lockActions && !flippingTurnRoutine)
        {
            flippingTurnRoutine = true;
            turnMarker.targetPosition = turnMarker.startPos;
            StartCoroutine(turnMarker.Flip());
        }

        if (enemyEntity != null && enemyHealth.value != enemyEntity.currentHealth / enemyEntity.maxHealth || allyHealth.value != allyEntity.currentHealth / allyEntity.maxHealth)
        {
            enemyHealth.value = enemyEntity.currentHealth / enemyEntity.maxHealth;
            allyHealth.value = allyEntity.currentHealth / allyEntity.maxHealth;
        }

        // poison checker
        if (allyEntity != null && allyEntity.poisonDamageAmount > 0)
        {
            allyPoisonIcon.SetActive(true);
            allyPoisonText.text = allyEntity.poisonDamageAmount.ToString();
        }
        else
            allyPoisonIcon.SetActive(false);


        if (enemyEntity != null && enemyEntity.poisonDamageAmount > 0)
        {
            enemyPoisonIcon.SetActive(true);
            enemyPoisonText.text = enemyEntity.poisonDamageAmount.ToString();
        }
        else
            enemyPoisonIcon.SetActive(false);
    }

    public void UpdateFlipState()
    {
        elipseFlipIcon.SetActive(numRemainingFlips > flipIcons.Count);
        foreach (GameObject icon in flipIcons)
        {
            icon.SetActive(false);
        }

        if (numRemainingFlips > flipIcons.Count)
        {
            for (int i = 0; i < flipIcons.Count; i++)
            {
                flipIcons[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < numRemainingFlips; i++)
            {
                flipIcons[i].SetActive(true);
            }
        }
    }

    public int GetAdjacentSlotDamages(int slotPos)
    {
        int retres = 0;
        if (slotPos - 1 >= 0 && coinRun[slotPos - 1].currentCoin.currentSide is DamagingAction)
            retres += (coinRun[slotPos - 1].currentCoin.currentSide as DamagingAction).damageAmount;
        if (slotPos + 1 < coinRun.Count && coinRun[slotPos + 1].currentCoin.currentSide is DamagingAction)
            retres += (coinRun[slotPos + 1].currentCoin.currentSide as DamagingAction).damageAmount;
        return retres;
    }

    public void StartTurn(Entity entity)
    {
        if (enemyEntity.currentHealth <= 0)// fix next turn
        {
            GameManager.money += enemyEntity.moneyOnDeath;
            DiscardNonSlots();
            DiscardSlots();
            StartCoroutine(SetupNextRound());
        }
        else if (allyEntity.currentHealth <= 0)
        {
            StartCoroutine(FadeToBlack());
        }
        else if (!lockTurnStarting)
        {
            currentEntityTurn = entity;
            currentEntityTurn.TurnStart();
            if (allyEntity == currentEntityTurn)
            {
                turnMarker.targetPosition = allyTurnMarkerPos.transform.position;
                turnMarker.SetSprite(true);
                StartCoroutine(StartShakeBag());
                numRemainingFlips = GameManager.numReFlips;
                UpdateFlipState();
            }
            else
            {
                turnMarker.targetPosition = enemyTurnMarkerPos.transform.position;
                turnMarker.SetSprite(false);
                StartCoroutine(enemyEntity.EnemyAttack());
            }
        }
    }

    public void LoadStartingCoins()
    {
        List<CoinBase> discardedCoins = new List<CoinBase>();
        foreach (CoinBase coin in GameManager.startingCoins)
        {
            GameObject newObj = new GameObject();
            newObj.tag = "Coin";
            GameObject childImage = new GameObject();
            childImage.transform.SetParent(newObj.transform);
            Animator anim = newObj.AddComponent(typeof(Animator)) as Animator;
            anim.runtimeAnimatorController = coinAnimator;
            Image image = newObj.AddComponent(typeof(Image)) as Image;
            Image icon = childImage.AddComponent(typeof(Image)) as Image;
            icon.raycastTarget = false;
            image.sprite = coinBase.GetComponent<Image>().sprite;
            RectTransform rectTransform = newObj.GetComponent<RectTransform>();
            RectTransform iconRectTransform = childImage.GetComponent<RectTransform>();


            iconRectTransform.sizeDelta = new Vector2((coinBase.transform as RectTransform).rect.width, (coinBase.transform as RectTransform).rect.height);
            rectTransform.sizeDelta = new Vector2((coinBase.transform as RectTransform).rect.width, (coinBase.transform as RectTransform).rect.height);

            newObj.name = coin.coinName;
            CoinBase newCoin = newObj.AddComponent(typeof(CoinBase)) as CoinBase;
            newCoin.coinName = coin.coinName;
            newCoin.tails = coin.tails;
            newCoin.heads = coin.heads;
            newCoin.speed = coin.speed;

            //AddCoinText
            GameObject topText = Instantiate(coinText);
            topText.transform.SetParent(newObj.transform);
            GameObject botText = Instantiate(coinText);
            botText.transform.SetParent(newObj.transform);
            (topText.transform as RectTransform).anchoredPosition += new Vector2(0, 17);
            (botText.transform as RectTransform).anchoredPosition -= new Vector2(0, 17);

            newCoin.topText = topText.GetComponent<TMP_Text>();
            newCoin.botText = botText.GetComponent<TMP_Text>();

            topText.gameObject.SetActive(false);
            botText.gameObject.SetActive(false);

            newCoin.distanceMultiplyer = coin.distanceMultiplyer;
            newCoin.dragSpeedMultiplyer = coin.dragSpeedMultiplyer;
            coinBagCoins.Add(newCoin);


            newCoin.gameObject.SetActive(false);
        }
    }
    IEnumerator FadeToBlack()
    {
        GameManager.Instance.FadeIn();
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("HubArea", LoadSceneMode.Single);
        yield return null;
    }

    IEnumerator SetupNextRound()
    {
        yield return ReshuffleDiscardedCoins();

        yield return SetupNextFight();

        yield return new WaitForSeconds(0.4f);

        turnMarker.targetPosition = turnMarker.startPos;
        yield return turnMarker.Flip();
    }

    IEnumerator SetupNextFight()
    {
        enemyEntity.targetPosition = spawner.spawnLocation.transform.position;
        StartCoroutine(enemyEntity.MoveToTarget());
        yield return new WaitForSeconds(1f);
        Destroy(enemyEntity.gameObject);
    }

    IEnumerator StartShakeBag()
    {
        // Check Bag Has Coins
        if (coinBagCoins.Count < GameManager.drawNumber)
        {
            List<CoinBase> discardedCoins = new List<CoinBase>();
            foreach (GameObject coinObj in GameObject.FindGameObjectsWithTag("Coin"))
            {
                coinObj.GetComponent<CoinBase>().targetPosition += new Vector2(0, -60);

                yield return new WaitForSeconds(0.1f);

                CoinBase coin = coinObj.GetComponent<CoinBase>();
                if (coin.discarded)
                {
                    GameObject newObj = new GameObject();
                    newObj.tag = "Coin";
                    GameObject childImage = new GameObject();
                    Animator anim = newObj.AddComponent(typeof(Animator)) as Animator;
                    anim.runtimeAnimatorController = coinAnimator;
                    childImage.transform.SetParent(newObj.transform);
                    Image image = newObj.AddComponent(typeof(Image)) as Image;
                    Image icon = childImage.AddComponent(typeof(Image)) as Image;
                    icon.raycastTarget = false;
                    image.sprite = coinBase.GetComponent<Image>().sprite;
                    RectTransform rectTransform = newObj.GetComponent<RectTransform>();
                    RectTransform iconRectTransform = childImage.GetComponent<RectTransform>();

                    iconRectTransform.sizeDelta = new Vector2((coinBase.transform as RectTransform).rect.width, (coinBase.transform as RectTransform).rect.height);
                    rectTransform.sizeDelta = new Vector2((coinBase.transform as RectTransform).rect.width, (coinBase.transform as RectTransform).rect.height);

                    newObj.name = coin.coinName;
                    CoinBase newCoin = newObj.AddComponent(typeof(CoinBase)) as CoinBase;
                    newCoin.coinName = coin.coinName;
                    newCoin.tails = coin.tails;
                    newCoin.heads = coin.heads;
                    newCoin.speed = coin.speed;

                    //AddCoinText
                    GameObject topText = Instantiate(coinText);
                    topText.transform.SetParent(newObj.transform);
                    GameObject botText = Instantiate(coinText);
                    botText.transform.SetParent(newObj.transform);
                    (topText.transform as RectTransform).anchoredPosition += new Vector2(0, 17);
                    (botText.transform as RectTransform).anchoredPosition -= new Vector2(0, 17);

                    topText.gameObject.SetActive(false);
                    botText.gameObject.SetActive(false);

                    newCoin.distanceMultiplyer = coin.distanceMultiplyer;
                    newCoin.dragSpeedMultiplyer = coin.dragSpeedMultiplyer;
                    coinBagCoins.Add(newCoin);
                    newCoin.gameObject.SetActive(false);


                    newCoin.topText = topText.GetComponent<TMP_Text>();
                    newCoin.botText = botText.GetComponent<TMP_Text>();

                    Destroy(coin.gameObject);
                }
            }
            yield return new WaitForSeconds(0.2f);
        }

        coinBagObject.GetComponent<Animator>().Play("Shake");
    }

    IEnumerator ReshuffleDiscardedCoins()
    {
        Debug.Log("Reshuffling");

        List<CoinBase> discardedCoins = new List<CoinBase>();
        foreach (GameObject coinObj in GameObject.FindGameObjectsWithTag("Coin"))
        {
            coinObj.GetComponent<CoinBase>().targetPosition += new Vector2(0, -60);

            yield return new WaitForSeconds(0.1f);

            CoinBase coin = coinObj.GetComponent<CoinBase>();
            if (coin.discarded)
            {
                GameObject newObj = new GameObject();
                newObj.tag = "Coin";
                GameObject childImage = new GameObject();
                Animator anim = newObj.AddComponent(typeof(Animator)) as Animator;
                anim.runtimeAnimatorController = coinAnimator;
                childImage.transform.SetParent(newObj.transform);
                Image image = newObj.AddComponent(typeof(Image)) as Image;
                Image icon = childImage.AddComponent(typeof(Image)) as Image;
                icon.raycastTarget = false;
                image.sprite = coinBase.GetComponent<Image>().sprite;
                RectTransform rectTransform = newObj.GetComponent<RectTransform>();
                RectTransform iconRectTransform = childImage.GetComponent<RectTransform>();

                iconRectTransform.sizeDelta = new Vector2((coinBase.transform as RectTransform).rect.width, (coinBase.transform as RectTransform).rect.height);
                rectTransform.sizeDelta = new Vector2((coinBase.transform as RectTransform).rect.width, (coinBase.transform as RectTransform).rect.height);

                newObj.name = coin.coinName;
                CoinBase newCoin = newObj.AddComponent(typeof(CoinBase)) as CoinBase;
                newCoin.coinName = coin.coinName;
                newCoin.tails = coin.tails;
                newCoin.heads = coin.heads;
                newCoin.speed = coin.speed;

                //AddCoinText
                GameObject topText = Instantiate(coinText);
                topText.transform.SetParent(newObj.transform);
                GameObject botText = Instantiate(coinText);
                botText.transform.SetParent(newObj.transform);
                (topText.transform as RectTransform).anchoredPosition += new Vector2(0, 17);
                (botText.transform as RectTransform).anchoredPosition -= new Vector2(0, 17);

                topText.gameObject.SetActive(false);
                botText.gameObject.SetActive(false);

                newCoin.distanceMultiplyer = coin.distanceMultiplyer;
                newCoin.dragSpeedMultiplyer = coin.dragSpeedMultiplyer;
                coinBagCoins.Add(newCoin);
                newCoin.gameObject.SetActive(false);


                newCoin.topText = topText.GetComponent<TMP_Text>();
                newCoin.botText = botText.GetComponent<TMP_Text>();

                Destroy(coin.gameObject);
            }
        }
    }

    public void SpawnCoins()
    {
        StartCoroutine(StartSpawningCoins(GameManager.drawNumber));
    }

    IEnumerator StartSpawningCoins(int numCoinsToSpawn)
    {
        int numSpawned = 0;
        Debug.Log("Started spawning coins");
        while (numSpawned < numCoinsToSpawn)
        {
            yield return new WaitForSeconds(1f / numCoinsToSpawn);
            GameObject tempObj = Instantiate(coinBagCoins[0].gameObject);
            coinBagCoins.RemoveAt(0);
            tempObj.transform.position = coinBagObject.transform.position;
            tempObj.transform.SetParent(this.transform);
            tempObj.transform.SetSiblingIndex(coinBagObject.transform.GetSiblingIndex() - 1);
            tempObj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            tempObj.SetActive(true);
            CoinBase newCoin = tempObj.GetComponent<CoinBase>();
            newCoin.targetPosition = GetRandPointInRect(coinArea);
            numSpawned++;
            Debug.Log("Coin spawned");
        }
        Debug.Log("Finished spawning coins");
    }

    Vector2 GetRandPointInRect(RectTransform rt)
    {
        // Get the rectangular bounding box of your UI element
        Rect rect = rt.rect;

        // Get the left, right, top, and bottom boundaries of the rect
        float leftSide = rt.anchoredPosition.x;
        float rightSide = rt.anchoredPosition.x + rect.width;
        float topSide = rt.anchoredPosition.y + rect.height;
        float bottomSide = rt.anchoredPosition.y;

        return new Vector2(Random.Range(leftSide, rightSide), Random.Range(bottomSide, topSide));
    }
    Vector2 GetRandPointInRectAnchoredBottomLeft(RectTransform rt)
    {
        // Get the rectangular bounding box of your UI element
        Rect rect = rt.rect;

        // Get the left, right, top, and bottom boundaries of the rect
        float leftSide = rt.anchoredPosition.x - 160;
        float rightSide = rt.anchoredPosition.x + rect.width - 160;
        float topSide = rt.anchoredPosition.y + rect.height - 90;
        float bottomSide = rt.anchoredPosition.y - 90;

        return new Vector2(Random.Range(leftSide, rightSide), Random.Range(bottomSide, topSide));
    }

    public void ValidateCoinRun()
    {
        bool res = true;
        foreach (CoinSlot slot in coinRun)
        {
            if (slot.currentCoin == null)
            {
                res = false;
            }
        }
        attackButton.interactable = res;
    }

    public void ResizeSlotArea()
    {
        while (coinRun.Count > 0)
        {
            CoinSlot temp = coinRun[0];
            Destroy(temp.gameObject);
            coinRun.RemoveAt(0);
        }

        for (int i = 0; i < numberOfSlots; i++)
        {
            GameObject newSlot = Instantiate(coinSlotBase);
            newSlot.transform.SetParent(this.transform);
            newSlot.transform.SetSiblingIndex(1);
            newSlot.transform.SetAsFirstSibling();
            RectTransform rectTransform = newSlot.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector2((i * rectTransform.rect.width) - (((numberOfSlots - 1f) / 2f) * rectTransform.rect.width) + (coinSlotMargin.x * i) - (((numberOfSlots - 1f) / 2f) * coinSlotMargin.x),
                (rectTransform.rect.height / 2) + coinSlotMargin.y);
            rectTransform.localScale = new Vector3(1, 1, 1);
            coinRun.Add(newSlot.GetComponent<CoinSlot>());
        }
    }

    public void StartCoinRun()
    {
        StartCoroutine(DoCoinRun());
        attackButton.interactable = false;
    }

    IEnumerator DoCoinRun()
    {
        int i = 0;

        yield return BeforeRun();
        float healthCheck = enemyEntity.currentHealth;
        while (i < coinRun.Count)
        {
            coinRun[i].currentCoin.currentSide.DoEffect();
            coinRun[i].PlayParticles();
            i++;
            yield return new WaitForSeconds(0.3f);
        }
        Camera.main.GetComponent<AudioSource>().PlayOneShot(GameAssets.Instance.soundAudioClips[5]);
        if (enemyEntity.GetComponent<Animator>() != null && healthCheck < enemyEntity.currentHealth)
            enemyEntity.GetComponent<Animator>().Play("EnemyWabble");

        yield return AfterRun();
        StartTurn(currentEntityTurn == (Entity)allyEntity ? (Entity)enemyEntity : (Entity)allyEntity);
    }

    IEnumerator BeforeRun()
    {
        List<CoinBase> noSlotCoins = new List<CoinBase>();
        foreach (GameObject coinObj in GameObject.FindGameObjectsWithTag("Coin"))
        {
            CoinBase coin = coinObj.GetComponent<CoinBase>();
            if (coin.slot == null && !coin.discarded)
            {
                noSlotCoins.Add(coin.GetComponent<CoinBase>());
                coin.targetPosition = GetRandPointInRectAnchoredBottomLeft(discardArea);
                coin.transform.SetParent(this.transform);
                coin.discarded = true;
            }
        }

        bool res = false;
        while (!res)
        {
            res = true;
            foreach (CoinBase coin in noSlotCoins)
            {
                if (coin.IsPointInRT((Vector2)coin.transform.position, discardArea))
                    res = false;
            }
            yield return new WaitForSeconds(0.2f);
        }
        yield return null; yield return BeforeRun();
        float healthCheck = enemyEntity.currentHealth;

        int i = 0;
        while (i < coinRun.Count)
        {
            coinRun[i].currentCoin.currentSide.PreRun(i);
            i++;
        }
    }

    private void DiscardNonSlots()
    {
        List<CoinBase> noSlotCoins = new List<CoinBase>();
        foreach (GameObject coinObj in GameObject.FindGameObjectsWithTag("Coin"))
        {
            CoinBase coin = coinObj.GetComponent<CoinBase>();
            if (coin.slot == null && !coin.discarded)
            {
                noSlotCoins.Add(coin.GetComponent<CoinBase>());
                coin.targetPosition = GetRandPointInRectAnchoredBottomLeft(discardArea);
                coin.transform.SetParent(this.transform);
                coin.discarded = true;
            }
        }
    }

    private void DiscardSlots()
    {
        List<CoinBase> noSlotCoins = new List<CoinBase>();
        foreach (GameObject coinObj in GameObject.FindGameObjectsWithTag("Coin"))
        {
            CoinBase coin = coinObj.GetComponent<CoinBase>();
            if (coin.slot != null && !coin.discarded)
            {
                noSlotCoins.Add(coin.GetComponent<CoinBase>());
                coin.targetPosition = GetRandPointInRectAnchoredBottomLeft(discardArea);
                coin.transform.SetParent(this.transform);
                coin.discarded = true;
            }
        }
    }

    IEnumerator AfterRun()
    {
        List<CoinBase> slotCoins = new List<CoinBase>();
        foreach (GameObject coinObj in GameObject.FindGameObjectsWithTag("Coin"))
        {
            CoinBase coin = coinObj.GetComponent<CoinBase>();
            if (coin.slot != null)
            {
                slotCoins.Add(coin.GetComponent<CoinBase>());
                coin.slot.currentCoin = null;
                coin.slot = null;
                coin.targetPosition = GetRandPointInRectAnchoredBottomLeft(discardArea);
                coin.transform.SetParent(this.transform);
                coin.discarded = true;
            }
        }

        bool res = false;
        while (!res)
        {
            res = true;
            foreach (CoinBase coin in slotCoins)
            {
                if (coin.IsPointInRT((Vector2)coin.transform.position, discardArea))
                    res = false;
            }
            yield return new WaitForSeconds(0.1f);
        }

        if (enemyEntity.currentHealth <= 0)
        {
            // Win and do win thing
            lockTurnStarting = true;
        }

        if (allyEntity.currentHealth <= 0)
        {
            // Go back to hub area  
            lockTurnStarting = true;
        }

        yield return null;
    }
}
