using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Heal Ally Action", menuName = "ScriptableObjects/NewHealAllyAction", order = 1)]
public class HealAlly : Action
{
    public int damageAmount;
    public override void DoEffect()
    {
        base.DoEffect();
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().allyEntity.currentHealth += damageAmount;
        if (GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().allyEntity.currentHealth > GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().allyEntity.maxHealth)
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().allyEntity.currentHealth += GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().allyEntity.maxHealth;
    }
}
