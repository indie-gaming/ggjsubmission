using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CoinBase : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public string coinName;
    public GameObject iconObj;
    public bool discarded;
    public bool dragging;

    // Positioning Variables
    public float speed = 1;
    public float distanceMultiplyer = 0.1f;
    public float dragSpeedMultiplyer = 8;
    public Vector2 targetPosition;
    public Vector2 returnPosition;
    private Canvas canvas;
    private RectTransform rectTransform;
    public RectTransform coinArea = null;
    public CoinSlot slot = null;

    public Action heads;
    public Action tails;

    public Action currentSide;

    public TMP_Text topText;
    public TMP_Text botText;
    public CoinBase(Action head, Action tail, string coinStr)
    {
        coinName = coinStr;
        heads = head;
        tails = tail;
    }

    private void Awake()
    {
        iconObj = this.transform.GetChild(0).gameObject;
        canvas = GameObject.FindGameObjectWithTag("UI").GetComponent<Canvas>();
        coinArea = GameObject.FindGameObjectWithTag("CoinArea").GetComponent<RectTransform>();
        rectTransform = this.GetComponent<RectTransform>();
        targetPosition = this.transform.position;
        Flip();
    }

    private void Update()
    {
        if ((Vector2)this.transform.position != targetPosition)
        {
            float moveSpeed = dragging ? speed * dragSpeedMultiplyer : speed;
            rectTransform.anchoredPosition = Vector2.MoveTowards(rectTransform.anchoredPosition, targetPosition, (moveSpeed +
                (Vector2.Distance(rectTransform.anchoredPosition, targetPosition) * distanceMultiplyer)));
        }
    }

    public void FlipFinished()
    {
        iconObj.SetActive(true);
    }

    public void Flip()
    {
        iconObj.SetActive(false);
        currentSide = Random.value >= 0.5f ? heads : tails;
        if (topText != null && botText != null)
        {
            if (currentSide == heads)
            {
                topText.text = heads.uiText;
                botText.text = tails.uiText;
            }
            else
            {
                botText.text = heads.uiText;
                topText.text = tails.uiText;
            }

        }
        //Play Anim Here
        SetImageUnlock();
    }

    public void SetImageUnlock()
    {
        if (currentSide != null)
        {
            iconObj.GetComponent<Image>().sprite = currentSide.icon;
            iconObj.GetComponent<Image>().color = Color.white;
        }
        else
        {
            iconObj.GetComponent<Image>().sprite = null;
            iconObj.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        }
        iconObj.SetActive(true);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (discarded || GameManager.Instance.lockActions)
            eventData.pointerDrag = null;
        else
        {
            this.GetComponent<Image>().raycastTarget = false;
            returnPosition = rectTransform.anchoredPosition;
            this.transform.SetParent(canvas.transform);
            dragging = true;
            this.transform.SetAsLastSibling();
            GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().attackButton.interactable = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out pos);

        if (dragging)
            targetPosition = ScreenMaths.ConvertScreenToCanvasPosition(canvas, Input.mousePosition); // Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (dragging)
        {
            this.GetComponent<Image>().raycastTarget = true;
            dragging = false;

            if (IsPointInRT(ScreenMaths.ConvertScreenToCanvasPosition(canvas, Input.mousePosition), coinArea))
            {
                returnPosition = ScreenMaths.ConvertScreenToCanvasPosition(canvas, Input.mousePosition);
                if (slot != null) slot.currentCoin = null;
                slot = null;
            }

            if (slot != null)
            {
                GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().ValidateCoinRun();
            }
            targetPosition = returnPosition;
        }
    }

    public bool IsPointInRT(Vector2 point, RectTransform rt)
    {
        // Get the rectangular bounding box of your UI element
        Rect rect = rt.rect;

        // Get the left, right, top, and bottom boundaries of the rect
        float leftSide = rt.anchoredPosition.x;
        float rightSide = rt.anchoredPosition.x + rect.width;
        float topSide = rt.anchoredPosition.y + rect.height;
        float bottomSide = rt.anchoredPosition.y;

        //Debug.Log(leftSide + ", " + rightSide + ", " + topSide + ", " + bottomSide);

        // Check to see if the point is in the calculated bounds
        if (point.x >= leftSide &&
            point.x <= rightSide &&
            point.y >= bottomSide &&
            point.y <= topSide)
        {
            return true;
        }
        return false;
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.clickCount == 2 && canvas.GetComponent<ActionOrganiser>().numRemainingFlips > 0)
        {
            Debug.Log("double click" + eventData.clickCount);
            eventData.clickTime = 0;
            canvas.GetComponent<ActionOrganiser>().numRemainingFlips--;
            canvas.GetComponent<ActionOrganiser>().UpdateFlipState();
            Flip();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(slot == null)
        {
            topText.gameObject.SetActive(true);
            botText.gameObject.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        topText.gameObject.SetActive(false);
        botText.gameObject.SetActive(false);
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(CoinBase))]
public class ObjectBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CoinBase myScript = (CoinBase)target;
        if (GUILayout.Button("Build Object"))
        {
            myScript.Flip();
        }
    }
}
#endif

