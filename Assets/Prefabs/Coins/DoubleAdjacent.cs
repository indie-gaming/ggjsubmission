using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Heal Enemy Action", menuName = "ScriptableObjects/NewHealEnemyAction", order = 1)]
public class DoubleAdjacent : Action
{
    public int damageAmount;
    public int currentDamageAmount;
    public override void PreRun(int runPos)
    {
        base.PreRun(runPos);
        currentDamageAmount = GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().GetAdjacentSlotDamages(runPos);
    }

    public override void DoEffect()
    {
        base.DoEffect();
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().enemyEntity.currentHealth -= damageAmount;
    }

}
