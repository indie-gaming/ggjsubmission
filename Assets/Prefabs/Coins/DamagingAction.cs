using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Damage Action", menuName = "ScriptableObjects/NewDamageAction", order = 1)]
public class DamagingAction : Action
{
    public int damageAmount;
    public override void DoEffect()
    {
        base.DoEffect();
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().enemyEntity.currentHealth -= damageAmount + GameManager.damageIncrease;
    }
}
