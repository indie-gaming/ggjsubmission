using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Poison Enemy Action", menuName = "ScriptableObjects/NewPoisonEnemyAction", order = 1)]
public class PoisonEnemy : Action
{
    public int damageAmount;
    public override void DoEffect()
    {
        base.DoEffect();
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().enemyEntity.poisonDamageAmount += damageAmount;
    }
}
