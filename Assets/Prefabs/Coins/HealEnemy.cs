using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Heal Enemy Action", menuName = "ScriptableObjects/NewHealEnemyAction", order = 1)]
public class HealEnemy : Action
{
    public int damageAmount;
    public override void DoEffect()
    {
        base.DoEffect();
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().enemyEntity.currentHealth += damageAmount;
        if (GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().enemyEntity.currentHealth > GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().enemyEntity.maxHealth)
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().enemyEntity.currentHealth += GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().enemyEntity.maxHealth;
    }
}
