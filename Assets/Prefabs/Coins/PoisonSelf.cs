using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PoisonSelf Action", menuName = "ScriptableObjects/NewPoisonSelfAction", order = 1)]
public class PosionSelf : Action
{
    public int damageAmount;
    public override void DoEffect()
    {
        base.DoEffect();
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().allyEntity.poisonDamageAmount += damageAmount;
    }
}
