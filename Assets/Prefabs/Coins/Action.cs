using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Action", menuName = "ScriptableObjects/NewAction", order = 1)]
public class Action : ScriptableObject
{
    public string name = "";
    public string uiText = "";
    public Sprite icon;

    public virtual void CoinModifiers(Transform coinTransform)
    {

    }

    public virtual void PreRun(int runPos)
    {

    }

    public virtual void DoEffect()
    {

    }
    public virtual void PostRun()
    {

    }
}
