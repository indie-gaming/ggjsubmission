using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Self Damage Action", menuName = "ScriptableObjects/NewSelfDamageAction", order = 1)]
public class DamageSelf : Action
{
    public int damageAmount;
    public override void DoEffect()
    {
        base.DoEffect();
        GameObject.FindGameObjectWithTag("UI").GetComponent<ActionOrganiser>().allyEntity.currentHealth -= damageAmount;
    }
}
