using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HubWorldManager : MonoBehaviour
{
    public GameObject moneyContainer;
    public TMP_Text moneyText;

    public GameObject witchHut;
    public GameObject armoury;

    public List<CoinBase> startGameCoins = new List<CoinBase>();
    void Awake()
    {
        if(GameManager.firstTimeLaunch) // TESTGJP add in
        {
            GameManager.startingCoins.AddRange(startGameCoins);
            startGameCoins.Clear();
        }

        moneyText.text = GameManager.money.ToString();
        if (GameManager.numFights > 0)
        {
            moneyContainer.SetActive(true);
            armoury.SetActive(true);
            // Show Story
        }
        if (GameManager.numFights > 1)
        {
            witchHut.SetActive(true);
        }

        SoundManager.PlayMusic(GameAssets.Instance.soundAudioClips[6]);
    }

    void Update()
    {
        moneyText.text = GameManager.money.ToString();
    }

    public void EnterBlackSmith()
    {

    }

    public void EnterWitchesHut()
    {

    }
}
